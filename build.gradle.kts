import com.google.protobuf.gradle.*

plugins {
    id("java")
    id("com.google.protobuf") version "0.9.3"
    id("com.github.johnrengelman.shadow") version ("8.1.1")

}

group = "io.gitlab.Adariel"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven("https://repo.aikar.co/content/groups/aikar/")
}

dependencies {
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    //GRPC
    implementation("io.grpc:grpc-netty-shaded:1.55.1")
    implementation("io.grpc:grpc-protobuf:1.55.1")
    implementation("io.grpc:grpc-stub:1.55.1")
    compileOnly("org.apache.tomcat:annotations-api:6.0.53")
    //PAPER
    compileOnly("io.papermc.paper:paper-api:1.19.4-R0.1-SNAPSHOT")
    //GUICE
    implementation("com.google.inject:guice:5.1.0")
    //LOMBOK
    compileOnly("org.projectlombok:lombok:1.18.26")
    annotationProcessor("org.projectlombok:lombok:1.18.26")
    testCompileOnly("org.projectlombok:lombok:1.18.26")
    testAnnotationProcessor("org.projectlombok:lombok:1.18.26")
    //ADVANCED COMMAND FRAMEWORK
    implementation("co.aikar:acf-paper:0.5.1-SNAPSHOT")
}


//CONFIGURA ADVANCED COMMAND FRAMEWORK

tasks.named<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar>("shadowJar") {
    relocate("co.aikar.commands", "io.gitlab.Adariel.acf")
    relocate("co.aikar.locales", "io.gitlab.Adariel.locales")
    mergeServiceFiles()
    append("META-INF/services/io.grpc.NameResolverProvider")
}

val cloneRepo by tasks.register<Exec>("cloneRepo") {
    workingDir("src/main")
    commandLine("git", "clone", "git@gitlab.com:kandor-rp-ecosystem/kandor-proto.git", "proto")
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:21.0-rc-1"
    }
    plugins {
        id("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:1.55.1"
        }
    }
    generateProtoTasks {
        ofSourceSet("main").forEach {
            it.plugins {
                id("grpc") {}
            }
        }
    }
}

tasks.test {
    useJUnitPlatform()
}

tasks.named<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar>("shadowJar") {
    val userHome = System.getProperty("user.home")
    val outputPath = File("$userHome/Escritorio/minecraft server/plugins")
    destinationDirectory.set(outputPath)
}

