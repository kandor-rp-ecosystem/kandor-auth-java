package io.gitlab.Adariel.commands.groups;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import com.google.inject.Inject;
import io.gitlab.Adariel.commands.Commands;
import io.gitlab.Adariel.commands.hanlders.RemovePlayerFromGroupHandler;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

@CommandAlias(Commands.GROUP_PARENT)
@Subcommand("removeplayer")
public class RemovePlayerFromGroupCmd extends BaseCommand {

    private final RemovePlayerFromGroupHandler removePlayerFromGroupHandler;

    @Inject
    public RemovePlayerFromGroupCmd(RemovePlayerFromGroupHandler removePlayerFromGroupHandler) {
        this.removePlayerFromGroupHandler = removePlayerFromGroupHandler;
    }

    @Default
    @Description("Add player to group")
    @CommandPermission("kandor.auth.group.removeplayer")
    public void removePlaerFromGroup(@NotNull Player player, String groupName, String playerNick) {
        removePlayerFromGroupHandler.removePlayer(player, groupName, playerNick);
    }
}
