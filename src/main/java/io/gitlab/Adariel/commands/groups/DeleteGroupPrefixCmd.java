package io.gitlab.Adariel.commands.groups;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import com.google.inject.Inject;
import io.gitlab.Adariel.commands.Commands;
import io.gitlab.Adariel.commands.hanlders.DeleteGroupHandler;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

@CommandAlias(Commands.GROUP_PARENT)
@Subcommand("deleteprefix")
public class DeleteGroupPrefixCmd extends BaseCommand {
    private final DeleteGroupHandler deleteGroupHandler;

    @Inject
    public DeleteGroupPrefixCmd(DeleteGroupHandler deleteGroupHandler) {
        this.deleteGroupHandler = deleteGroupHandler;
    }

    @Default
    @Description("Delete group prefix")
    @CommandPermission("kandor.auth.group.deleteprefix")
    @CommandCompletion("group_name")
    public void deletePrefix(@NotNull Player player, String groupName) {
        deleteGroupHandler.deleteGroup(player, groupName);
    }

}
