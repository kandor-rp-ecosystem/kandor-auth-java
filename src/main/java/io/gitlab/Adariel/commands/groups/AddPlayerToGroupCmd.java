package io.gitlab.Adariel.commands.groups;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import com.google.inject.Inject;
import io.gitlab.Adariel.commands.Commands;
import io.gitlab.Adariel.commands.hanlders.AddPlayerToGroupHandler;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

@CommandAlias(Commands.GROUP_PARENT)
@Subcommand("addplayer")
public class AddPlayerToGroupCmd extends BaseCommand {

    private final AddPlayerToGroupHandler addPlayerToGroupHandler;

    @Inject
    public AddPlayerToGroupCmd(AddPlayerToGroupHandler addPlayerToGroupHandler) {
        this.addPlayerToGroupHandler = addPlayerToGroupHandler;
    }


    @Default
    @Description("Add player to group")
    @CommandPermission("kandor.auth.group.addplayer")
    @CommandCompletion("group_name")
    public void addPlayerToGroup(@NotNull Player player, String groupName, String playerNick) {
        addPlayerToGroupHandler.addPlayer(groupName, playerNick, player);
    }
}