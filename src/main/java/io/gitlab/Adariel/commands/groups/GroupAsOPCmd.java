package io.gitlab.Adariel.commands.groups;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import com.google.inject.Inject;
import io.gitlab.Adariel.commands.Commands;
import io.gitlab.Adariel.commands.hanlders.GroupAsOPHandler;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

@CommandAlias(Commands.GROUP_PARENT)
@Subcommand("setop")
public class GroupAsOPCmd extends BaseCommand {

    private final GroupAsOPHandler groupAsOPHandler;

    @Inject
    public GroupAsOPCmd(GroupAsOPHandler groupAsOPHandler) {
        this.groupAsOPHandler = groupAsOPHandler;
    }

    @Default
    @Description("Set group as op")
    @CommandPermission("kandor.auth.group.setop")
    @CommandCompletion("group_name")
    public void setGroupAsOp(@NotNull Player player, String groupName) {
        groupAsOPHandler.setGroupAsOP(player, groupName);
    }
}

