package io.gitlab.Adariel.commands.groups;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import com.google.inject.Inject;
import io.gitlab.Adariel.commands.Commands;
import io.gitlab.Adariel.commands.hanlders.AddPermToGroupHandler;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

@CommandAlias(Commands.GROUP_PARENT)
@Subcommand("add")
public class AddPermToGroupCmd extends BaseCommand {
    private final AddPermToGroupHandler addPermToGroupHandler;

    @Inject
    public AddPermToGroupCmd(AddPermToGroupHandler addPermToGroupHandler) {
        this.addPermToGroupHandler = addPermToGroupHandler;
    }

    @Default
    @Description("Add permission to group")
    @CommandPermission("kandor.auth.group.addperm")
    @CommandCompletion("group_name permission_name")
    public void addPermToGroup(@NotNull Player player, String groupName, String permissionName) {
        addPermToGroupHandler.addPerm(groupName, permissionName, player);
    }
}
