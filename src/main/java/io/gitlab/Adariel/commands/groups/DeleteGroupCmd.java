package io.gitlab.Adariel.commands.groups;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import com.google.inject.Inject;
import io.gitlab.Adariel.commands.Commands;
import io.gitlab.Adariel.commands.hanlders.DeleteGroupHandler;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

@CommandAlias(Commands.GROUP_PARENT)
@Subcommand("delete")
public class DeleteGroupCmd extends BaseCommand {

    private final DeleteGroupHandler deleteGroupHandler;

    @Inject
    public DeleteGroupCmd(DeleteGroupHandler deleteGroupHandler) {
        this.deleteGroupHandler = deleteGroupHandler;
    }

    @Default
    @Description("Delete a group")
    @CommandPermission("kandor.auth.group.delete")
    @CommandCompletion("group_name")
    public void deleteGroup(@NotNull Player player, String groupName) {
        deleteGroupHandler.deleteGroup(player, groupName);
    }
}
