package io.gitlab.Adariel.commands.groups;


import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import com.google.inject.Inject;
import io.gitlab.Adariel.commands.Commands;
import io.gitlab.Adariel.commands.hanlders.SetGroupSuffixHandler;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

@CommandAlias(Commands.GROUP_PARENT)
@Subcommand("setsuffix")
public class SetGroupSuffixCmd extends BaseCommand {

    private final SetGroupSuffixHandler setGroupSuffixHandler;

    @Inject
    public SetGroupSuffixCmd(SetGroupSuffixHandler setGroupSuffixHandler) {
        this.setGroupSuffixHandler = setGroupSuffixHandler;
    }

    @Default
    @Description("Add group suffix")
    @CommandPermission("kandor.auth.group.setsuffix")
    @CommandCompletion("group_name suffix")
    public void setSuffix(@NotNull Player player, String groupName, String suffix) {
        setGroupSuffixHandler.setSuffix(player, groupName, suffix);
    }
}
