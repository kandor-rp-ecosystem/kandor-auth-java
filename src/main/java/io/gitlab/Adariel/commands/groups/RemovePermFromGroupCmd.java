package io.gitlab.Adariel.commands.groups;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import com.google.inject.Inject;
import io.gitlab.Adariel.commands.Commands;
import io.gitlab.Adariel.commands.hanlders.RemovePermFromGroupHandler;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

@CommandAlias(Commands.GROUP_PARENT)
@Subcommand("remove")
public class RemovePermFromGroupCmd extends BaseCommand {

    private final RemovePermFromGroupHandler removePermFromGroupHandler;

    @Inject
    public RemovePermFromGroupCmd(RemovePermFromGroupHandler removePermFromGroupHandler) {
        this.removePermFromGroupHandler = removePermFromGroupHandler;
    }

    @Default
    @Description("Add permission to group")
    @CommandPermission("kandor.auth.group.remove")
    @CommandCompletion("group_name")
    public void addPermToGroup(@NotNull Player player, String groupName, String permissionName) {
        removePermFromGroupHandler.addPerm(player, groupName, permissionName);
    }
}
