package io.gitlab.Adariel.commands.groups;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import com.google.inject.Inject;
import io.gitlab.Adariel.commands.Commands;
import io.gitlab.Adariel.commands.hanlders.DeleteGroupSuffixHandler;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

@CommandAlias(Commands.GROUP_PARENT)
@Subcommand("deletesuffix")
public class DeleteGroupSuffixCmd extends BaseCommand {
    private final DeleteGroupSuffixHandler deleteGroupSuffixHandler;

    @Inject
    public DeleteGroupSuffixCmd(DeleteGroupSuffixHandler deleteGroupSuffixHandler) {
        this.deleteGroupSuffixHandler = deleteGroupSuffixHandler;
    }

    @Default
    @Description("Delete group suffix")
    @CommandPermission("kandor.auth.group.deletesuffix")
    @CommandCompletion("group_name")
    public void deleteGroupSuffix(@NotNull Player player, String groupName) {
        deleteGroupSuffixHandler.deleteSuffix(player, groupName);
    }
}
