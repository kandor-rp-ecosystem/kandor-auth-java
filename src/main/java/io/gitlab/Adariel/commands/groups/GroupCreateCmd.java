package io.gitlab.Adariel.commands.groups;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import com.google.inject.Inject;
import io.gitlab.Adariel.commands.Commands;
import io.gitlab.Adariel.commands.hanlders.GroupCreateHandler;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

@CommandAlias(Commands.GROUP_PARENT)
@Subcommand("create")
public class GroupCreateCmd extends BaseCommand {

    private final GroupCreateHandler groupCreateHandler;

    @Inject
    public GroupCreateCmd(GroupCreateHandler groupCreateHandler) {
        this.groupCreateHandler = groupCreateHandler;
    }

    @Default
    @Description("Create a group")
    @CommandPermission("kandor.auth.group.create")
    @CommandCompletion("group_name group_template")
    public void createGroup(@NotNull Player player, String groupName, @Optional String groupTemplate) {
        if (groupTemplate != null) {
            groupCreateHandler.createGroupWithTemplate(player, groupName, groupTemplate);
        } else {
            groupCreateHandler.createGroupWithoutTemplate(player, groupName);
        }
    }
}
