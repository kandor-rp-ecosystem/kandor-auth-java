package io.gitlab.Adariel.commands.groups;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import com.google.inject.Inject;
import io.gitlab.Adariel.commands.Commands;
import io.gitlab.Adariel.commands.hanlders.SetGroupPrefixHandler;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

@CommandAlias(Commands.GROUP_PARENT)
@Subcommand("setprefix")
public class SetGroupPrefixCmd extends BaseCommand {

    private final SetGroupPrefixHandler setGroupPrefixHandler;

    @Inject
    public SetGroupPrefixCmd(SetGroupPrefixHandler setGroupPrefixHandler) {
        this.setGroupPrefixHandler = setGroupPrefixHandler;
    }

    @Default
    @Description("Add group prefix")
    @CommandPermission("kandor.auth.group.setprefix")
    @CommandCompletion("group_name prefix")
    public void setPrefix(@NotNull Player player, String groupName, String prefix) {
        setGroupPrefixHandler.setPrefix(player, groupName, prefix);
    }
}
