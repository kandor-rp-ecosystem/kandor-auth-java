package io.gitlab.Adariel.commands.groups;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Description;
import io.gitlab.Adariel.commands.Commands;
import net.kyori.adventure.text.Component;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

@CommandAlias(Commands.GROUP_PARENT)
public class GroupCmd extends BaseCommand {

    @Default
    @Description("Group help")
    //@CommandPermission("kandor.auth.group.help")
    public void showHelp(@NotNull Player help) {
        help.sendMessage(Component.text("Comando group"));
    }

    //TODO: setdefault
    //TODO: getprefix
    //TODO: getplayers
    //TODO: getgroups
    //TODO: getsuffix
}
