package io.gitlab.Adariel.commands.hanlders;

import com.google.inject.Inject;
import groups.Groups;
import io.gitlab.Adariel.exceptions.GrpcExceptionHandler;
import io.gitlab.Adariel.observers.ActionableObserver;
import io.gitlab.Adariel.observers.groups.UpdatedGroupObserver;
import io.gitlab.Adariel.services.GroupService;
import io.gitlab.Adariel.utils.ProtoUtils;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class DeleteGroupPrefixHandler {
    Groups.Group group;
    private final GroupService groupService;

    @Inject
    public DeleteGroupPrefixHandler(GroupService groupService) {
        this.groupService = groupService;
    }

    public void deleteGroupPrefix(@NotNull Player player, String groupName) {
        groupService.findGroupByName(Groups.GroupName.newBuilder().setGroupName(groupName).build(),
                new ActionableObserver<>(
                        response -> {
                            group = response;
                            updateGroup(player);
                        },
                        throwable -> {
                            new GrpcExceptionHandler(throwable, "This group not exists", player)
                                    .handleGrpcException();
                        },
                        () -> System.out.println("Borrado el prefijo del grupo")
                )
        );
    }

    private void updateGroup(Player player) {
        groupService.updateGroup(
                Groups.UpdateGroupDto
                        .newBuilder()
                        .setIdGroup(group.getIdGroup())
                        .setPrefix(ProtoUtils.EMPTY_STRING)
                        .setPermissions(
                                Groups.PermissionsRelations
                                        .newBuilder()
                                        .setProvided(false)
                                        .build())
                        .build(),
                new UpdatedGroupObserver(
                        player,
                        "The prefix has been successfully removed"
                )
        );
    }
}
