package io.gitlab.Adariel.commands.hanlders;

import com.google.inject.Inject;
import groups.Groups;
import io.gitlab.Adariel.exceptions.GrpcExceptionHandler;
import io.gitlab.Adariel.observers.ActionableObserver;
import io.gitlab.Adariel.observers.groups.UpdatedGroupObserver;
import io.gitlab.Adariel.services.GroupService;
import io.gitlab.Adariel.utils.ProtoUtils;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class DeleteGroupSuffixHandler {
    Groups.Group group;

    private final GroupService groupService;

    @Inject
    public DeleteGroupSuffixHandler(GroupService groupService) {
        this.groupService = groupService;
    }

    public void deleteSuffix(@NotNull Player player, String groupName) {
        groupService.findGroupByName(Groups.GroupName.newBuilder().setGroupName(groupName).build(),
                new ActionableObserver<>(
                        response -> {
                            updateGroup(player);
                        },
                        throwable -> {
                            new GrpcExceptionHandler(throwable, "This group not exists", player)
                                    .handleGrpcException();
                        },
                        () -> System.out.println("Sufijo eliminado")
                )
        );
    }

    private void updateGroup(Player player) {
        groupService.updateGroup(
                Groups.UpdateGroupDto
                        .newBuilder()
                        .setIdGroup(group.getIdGroup())
                        .setSuffix(ProtoUtils.EMPTY_STRING)
                        .setPermissions(
                                Groups.PermissionsRelations
                                        .newBuilder()
                                        .setProvided(false)
                                        .build())
                        .build(),
                new UpdatedGroupObserver(
                        player,
                        "The suffix has been successfully removed"
                )
        );
    }
}
