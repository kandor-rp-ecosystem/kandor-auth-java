package io.gitlab.Adariel.commands.hanlders;

import com.google.inject.Inject;
import groups.Groups;
import io.gitlab.Adariel.exceptions.GrpcExceptionHandler;
import io.gitlab.Adariel.observers.ActionableObserver;
import io.gitlab.Adariel.observers.groups.UpdatedGroupObserver;
import io.gitlab.Adariel.services.GroupService;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class SetGroupPrefixHandler {
    Groups.Group group;
    private final GroupService groupService;

    @Inject
    public SetGroupPrefixHandler(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setPrefix(@NotNull Player player, String groupName, String prefix) {
        groupService.findGroupByName(
                Groups.GroupName
                        .newBuilder()
                        .setGroupName(groupName)
                        .build(),
                new ActionableObserver<>(
                        response -> {
                            group = response;
                            findGroupByName(player, prefix);
                        },
                        throwable -> {
                            new GrpcExceptionHandler(throwable, "This group not exists", player)
                                    .handleGrpcException();
                        },
                        () -> System.out.println("Prefijo añadido")
                ));
    }

    private void findGroupByName(Player player, String prefix) {
        groupService.updateGroup(
                Groups.UpdateGroupDto
                        .newBuilder()
                        .setIdGroup(group.getIdGroup())
                        .setPrefix(prefix)
                        .setPermissions(
                                Groups.PermissionsRelations
                                        .newBuilder()
                                        .setProvided(false)
                                        .build())
                        .build(),
                new UpdatedGroupObserver(
                        player,
                        "The prefix has been successfully updated"
                )
        );
    }
}
