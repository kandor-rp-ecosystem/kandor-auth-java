package io.gitlab.Adariel.commands.hanlders;

import com.google.inject.Inject;
import groups.Groups;
import io.gitlab.Adariel.exceptions.GrpcExceptionHandler;
import io.gitlab.Adariel.observers.ActionableObserver;
import io.gitlab.Adariel.observers.groups.UpdatedGroupObserver;
import io.gitlab.Adariel.services.GroupService;
import io.gitlab.Adariel.services.PermissionService;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import permissions.Permissions;

import java.util.ArrayList;
import java.util.List;

public class RemovePermFromGroupHandler {
    Groups.Group group;
    Permissions.Permission permission;
    private final PermissionService permissionService;
    private final GroupService groupService;

    @Inject
    public RemovePermFromGroupHandler(PermissionService permissionService, GroupService groupService) {
        this.permissionService = permissionService;
        this.groupService = groupService;
    }

    public void addPerm(@NotNull Player player, String groupName, String permissionName) {
        groupService.findGroupByName(
                Groups.GroupName
                        .newBuilder()
                        .setGroupName(groupName)
                        .build(),
                new ActionableObserver<>(
                        response -> {
                            group = response;
                            findPermission(player, permissionName);
                        },
                        throwable -> {
                            new GrpcExceptionHandler(throwable, "This group not exists", player)
                                    .handleGrpcException();
                        },
                        () -> System.out.println("Permiso añadido")
                ));
    }

    private void findPermission(@NotNull Player player, String permissionName) {
        permissionService.findPermissionByName(
                Permissions.PermissionName.newBuilder()
                        .setPermissionName(permissionName)
                        .build(),
                new ActionableObserver<>(
                        response -> {
                            permission = response;
                            updateGroup(player);
                        },
                        throwable -> {
                            new GrpcExceptionHandler(throwable, "This permission not exists", player)
                                    .handleGrpcException();
                        },
                        () -> System.out.println("Grupo añadido")
                ));
    }

    private void updateGroup(Player player) {
        List<Integer> permissionsList = new ArrayList<>(group.getPermissionsList());
        int index = permissionsList.indexOf(permission.getIdPermission());
        if (index != -1) {
            permissionsList.remove(index);
        }
        groupService.updateGroup(
                Groups.UpdateGroupDto
                        .newBuilder()
                        .setIdGroup(group.getIdGroup())
                        .setPermissions(
                                Groups.PermissionsRelations
                                        .newBuilder()
                                        .setProvided(true)
                                        .setIsEmpty(permissionsList.isEmpty())
                                        .addAllIds(permissionsList)
                                        .build())
                        .build(),
                new UpdatedGroupObserver(
                        player,
                        "The permission has been removed successfully"
                )
        );
    }

}
