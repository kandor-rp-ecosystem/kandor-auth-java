package io.gitlab.Adariel.commands.hanlders;

import com.google.inject.Inject;
import groups.Groups;
import io.gitlab.Adariel.exceptions.GrpcExceptionHandler;
import io.gitlab.Adariel.observers.ActionableObserver;
import io.gitlab.Adariel.observers.groups.UpdatedGroupObserver;
import io.gitlab.Adariel.services.GroupService;
import io.gitlab.Adariel.services.PermissionService;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import permissions.Permissions;

import java.util.ArrayList;
import java.util.List;

public class GroupAsOPHandler {
    Groups.Group group;
    Permissions.Permission permission;
    private final PermissionService permissionService;
    private final GroupService groupService;

    @Inject
    public GroupAsOPHandler(PermissionService permissionService, GroupService groupService) {
        this.permissionService = permissionService;
        this.groupService = groupService;
    }

    public void setGroupAsOP(@NotNull Player player, String groupName) {
        groupService.findGroupByName(Groups.GroupName.newBuilder().setGroupName(groupName).build(),
                new ActionableObserver<>(
                        response -> {
                            group = response;
                            findPermissionById(player);
                        },
                        throwable -> {
                            new GrpcExceptionHandler(throwable, "This group not exists", player)
                                    .handleGrpcException();
                        },
                        () -> System.out.println("Grupo fijado como administrador")
                )
        );
    }

    private void findPermissionById(Player player) {
        permissionService.findPermissionByName(
                Permissions.PermissionName.newBuilder()
                        .setPermissionName("*")
                        .build(),
                new ActionableObserver<>(
                        response -> {
                            permission = response;
                            updateGroup(player);
                        },
                        Throwable::printStackTrace,
                        () -> System.out.println("Grupo fijado como administrador")
                )
        );
    }

    private void updateGroup(Player player) {
        List<Integer> permissionsList = new ArrayList<>(group.getPermissionsList());
        permissionsList.add(permission.getIdPermission());
        groupService.updateGroup(
                Groups.UpdateGroupDto
                        .newBuilder()
                        .setIdGroup(group.getIdGroup())
                        .setPermissions(
                                Groups.PermissionsRelations
                                        .newBuilder()
                                        .setIsEmpty(false)
                                        .setProvided(true)
                                        .addAllIds(permissionsList)
                                        .build())
                        .build(),
                new UpdatedGroupObserver(player, "The group is now admin")
        );
    }
}
