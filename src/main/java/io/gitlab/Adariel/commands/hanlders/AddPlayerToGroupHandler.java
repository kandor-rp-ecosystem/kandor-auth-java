package io.gitlab.Adariel.commands.hanlders;

import accounts.Accounts;
import com.google.inject.Inject;
import groups.Groups;
import io.gitlab.Adariel.exceptions.GrpcExceptionHandler;
import io.gitlab.Adariel.observers.ActionableObserver;
import io.gitlab.Adariel.observers.accounts.UpdateAccountObserver;
import io.gitlab.Adariel.services.AccountService;
import io.gitlab.Adariel.services.GroupService;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class AddPlayerToGroupHandler {
    Groups.Group group;
    Accounts.Account account;

    private final AccountService accountService;
    private final GroupService groupService;

    @Inject
    public AddPlayerToGroupHandler(AccountService accountService, GroupService groupService) {
        this.accountService = accountService;
        this.groupService = groupService;
    }

    public void addPlayer(String groupName, String playerNick, Player player) {
        groupService.findGroupByName(Groups.GroupName.newBuilder().setGroupName(groupName).build(),
                new ActionableObserver<>(
                        response -> {
                            group = response;
                            findAccountByNick(playerNick, player);
                        },
                        throwable -> {
                            new GrpcExceptionHandler(throwable, "This group not exists", player)
                                    .handleGrpcException();
                        },
                        () -> System.out.println("Jugador añadido al grupo")
                )
        );
    }

    public void findAccountByNick(String playerNick, Player player) {
        accountService.findAccountByNick(
                Accounts.AccountNick.newBuilder().setNick(playerNick).build(),
                new ActionableObserver<>(
                        response -> {
                            account = response;
                            updateAccount(player);
                        },
                        throwable -> {
                            new GrpcExceptionHandler(throwable, "This account not exists", player)
                                    .handleGrpcException();
                        },
                        () -> System.out.println("Jugador añadido al grupo")
                )
        );
    }

    public void updateAccount(Player player) {
        List<Integer> groupList = new ArrayList<>(account.getGroupsList());
        groupList.add(group.getIdGroup());
        accountService.updateAccount(
                Accounts.UpdateAccountDto
                        .newBuilder()
                        .setUuid(account.getUuid())
                        .setGroups(Accounts.GroupsRelations
                                .newBuilder()
                                .setProvided(true)
                                .setIsEmpty(false)
                                .addAllIds(groupList)
                                .build())
                        .build(), new UpdateAccountObserver(
                        player,
                        "The player has been added to group"));
    }
}
