package io.gitlab.Adariel.commands.hanlders;

import com.google.inject.Inject;
import groups.Groups;
import io.gitlab.Adariel.exceptions.GrpcExceptionHandler;
import io.gitlab.Adariel.observers.ActionableObserver;
import io.gitlab.Adariel.observers.groups.UpdatedGroupObserver;
import io.gitlab.Adariel.services.GroupService;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class SetGroupSuffixHandler {
    Groups.Group group;
    private final GroupService groupService;

    @Inject
    public SetGroupSuffixHandler(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setSuffix(@NotNull Player player, String groupName, String suffix) {
        groupService.findGroupByName(
                Groups.GroupName
                        .newBuilder()
                        .setGroupName(groupName)
                        .build(),
                new ActionableObserver<>(
                        response -> {
                            group = response;
                            updateGroup(player, suffix);
                        },
                        throwable -> {
                            new GrpcExceptionHandler(throwable, "This group not exists", player)
                                    .handleGrpcException();
                        },
                        () -> System.out.println("Sufijo establecido")
                ));
    }

    public void updateGroup(Player player, String suffix) {
        groupService.updateGroup(
                Groups.UpdateGroupDto
                        .newBuilder()
                        .setIdGroup(group.getIdGroup())
                        .setSuffix(suffix)
                        .setPermissions(
                                Groups.PermissionsRelations
                                        .newBuilder()
                                        .setProvided(false)
                                        .build())
                        .build(),
                new UpdatedGroupObserver(
                        player,
                        "The suffix has been successfully updated"
                )
        );
    }
}
