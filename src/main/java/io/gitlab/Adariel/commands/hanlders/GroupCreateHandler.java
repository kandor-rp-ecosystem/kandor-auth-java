package io.gitlab.Adariel.commands.hanlders;

import co.aikar.commands.annotation.Optional;
import com.google.inject.Inject;
import groups.Groups;
import io.gitlab.Adariel.exceptions.GrpcExceptionHandler;
import io.gitlab.Adariel.observers.ActionableObserver;
import io.gitlab.Adariel.observers.groups.CreatedGroupObserver;
import io.gitlab.Adariel.services.GroupService;
import io.gitlab.Adariel.utils.TextUtils;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class GroupCreateHandler {
    Groups.Group group;
    private final GroupService groupService;

    @Inject
    public GroupCreateHandler(GroupService groupService) {
        this.groupService = groupService;
    }

    public void createGroupWithoutTemplate(@NotNull Player player, String groupName) {
        groupService.createGroup(
                Groups.CreateGroupDto
                        .newBuilder()
                        .setName(groupName)
                        .setPermissions(
                                Groups.PermissionsRelations
                                        .newBuilder()
                                        .setProvided(false)
                                        .build()
                        )
                        .build(),
                new ActionableObserver<>(
                        response -> {
                            player.sendMessage(TextUtils.createSuccessComponent("The group has been created"));
                        },
                        Throwable::printStackTrace,
                        () -> System.out.println("Grupo creadp")
                ));
    }

    public void createGroupWithTemplate(@NotNull Player player, String groupName, @Optional String groupTemplate) {
        groupService.findGroupByName(
                Groups.GroupName
                        .newBuilder()
                        .setGroupName(groupTemplate)
                        .build(),
                new ActionableObserver<>(
                        response -> {
                            group = response;
                            createGroup(player, groupName);
                        },
                        throwable -> {
                            new GrpcExceptionHandler(throwable, "This group not exists", player)
                                    .handleGrpcException();
                        },
                        () -> System.out.println("Grupo creado desde plantilla")
                )
        );
    }

    private void createGroup(Player player, String groupName) {
        Groups.CreateGroupDto groupDto = Groups.CreateGroupDto
                .newBuilder()
                .setName(groupName)
                .setPrefix(group.getPrefix())
                .setSuffix(group.getSuffix())
                .setPermissions(Groups.PermissionsRelations.newBuilder()
                        .setProvided(true)
                        .setIsEmpty(group.getPermissionsList().isEmpty())
                        .addAllIds(group.getPermissionsList())
                        .build())
                .build();
        groupService.createGroup(groupDto, new CreatedGroupObserver(
                        player,
                        "The group has been created"
                )
        );
    }
}

