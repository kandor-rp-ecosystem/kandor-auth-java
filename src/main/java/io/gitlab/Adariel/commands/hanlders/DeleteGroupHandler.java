package io.gitlab.Adariel.commands.hanlders;

import com.google.inject.Inject;
import groups.Groups;
import io.gitlab.Adariel.exceptions.GrpcExceptionHandler;
import io.gitlab.Adariel.observers.ActionableObserver;
import io.gitlab.Adariel.observers.groups.DeletedGroupObserver;
import io.gitlab.Adariel.services.GroupService;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class DeleteGroupHandler {
    Groups.Group group;
    private final GroupService groupService;

    @Inject
    public DeleteGroupHandler(GroupService groupService) {
        this.groupService = groupService;
    }

    public void deleteGroup(@NotNull Player player, String groupName) {
        groupService.findGroupByName(Groups.GroupName.newBuilder().setGroupName(groupName).build(),
                new ActionableObserver<>(
                        response -> {
                            group = response;
                            updateGroup(player);
                        },
                        throwable -> {
                            new GrpcExceptionHandler(throwable, "This group not exists", player)
                                    .handleGrpcException();
                        },
                        () -> System.out.println("Grupo borrado")
                )
        );
    }

    public void updateGroup(Player player) {
        groupService.deleteGroup(
                Groups.GroupId
                        .newBuilder()
                        .setIdGroup(group.getIdGroup())
                        .build(),
                new DeletedGroupObserver(player, "The group has been deleted")
        );
    }
}
