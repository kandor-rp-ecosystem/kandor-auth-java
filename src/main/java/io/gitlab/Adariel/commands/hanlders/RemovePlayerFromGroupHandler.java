package io.gitlab.Adariel.commands.hanlders;

import accounts.Accounts;
import com.google.inject.Inject;
import groups.Groups;
import io.gitlab.Adariel.exceptions.GrpcExceptionHandler;
import io.gitlab.Adariel.observers.ActionableObserver;
import io.gitlab.Adariel.observers.accounts.UpdateAccountObserver;
import io.gitlab.Adariel.services.AccountService;
import io.gitlab.Adariel.services.GroupService;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class RemovePlayerFromGroupHandler {
    Groups.Group group;
    Accounts.Account account;
    private final AccountService accountService;
    private final GroupService groupService;

    @Inject
    public RemovePlayerFromGroupHandler(AccountService accountService, GroupService groupService) {
        this.accountService = accountService;
        this.groupService = groupService;
    }

    public void removePlayer(@NotNull Player player, String groupName, String playerNick) {
        groupService.findGroupByName(Groups.GroupName.newBuilder().setGroupName(groupName).build(),
                new ActionableObserver<>(
                        response -> {
                            group = response;
                            findAccountByNick(player, playerNick);
                        },
                        throwable -> {
                            new GrpcExceptionHandler(throwable, "This group not exists", player)
                                    .handleGrpcException();
                        },
                        () -> System.out.println("Quitado permiso al jugador")
                ));
    }

    private void findAccountByNick(@NotNull Player player, String playerNick) {
        accountService.findAccountByNick(
                Accounts.AccountNick.newBuilder().setNick(playerNick).build(),
                new ActionableObserver<>(
                        response -> {
                            account = response;
                            updateAccount(player);
                        },
                        throwable -> {
                            new GrpcExceptionHandler(throwable, "This account not exists", player)
                                    .handleGrpcException();
                        },
                        () -> System.out.println("Quitado permiso al jugador")
                ));
    }

    private void updateAccount(@NotNull Player player) {
        List<Integer> groupList = new ArrayList<>(account.getGroupsList());
        int index = groupList.indexOf(group.getIdGroup());
        if (index != -1) {
            groupList.remove(index);
        }
        accountService.updateAccount(
                Accounts.UpdateAccountDto
                        .newBuilder()
                        .setUuid(account.getUuid())
                        .setGroups(Accounts.GroupsRelations
                                .newBuilder()
                                .setProvided(true)
                                .setIsEmpty(groupList.isEmpty())
                                .addAllIds(groupList)
                                .build())
                        .build(), new UpdateAccountObserver(
                        player,
                        "The player has been removed from group"));
    }
}
