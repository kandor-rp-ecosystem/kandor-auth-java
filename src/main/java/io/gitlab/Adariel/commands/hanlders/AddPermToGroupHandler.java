package io.gitlab.Adariel.commands.hanlders;

import com.google.inject.Inject;
import groups.Groups;
import io.gitlab.Adariel.exceptions.GrpcExceptionHandler;
import io.gitlab.Adariel.observers.ActionableObserver;
import io.gitlab.Adariel.observers.groups.UpdatedGroupObserver;
import io.gitlab.Adariel.services.GroupService;
import io.gitlab.Adariel.services.PermissionService;
import org.bukkit.entity.Player;
import permissions.Permissions;

import java.util.ArrayList;
import java.util.List;

public class AddPermToGroupHandler {
    Groups.Group group;
    Permissions.Permission permission;
    private final PermissionService permissionService;
    private final GroupService groupService;

    @Inject
    public AddPermToGroupHandler(PermissionService permissionService, GroupService groupService) {
        this.permissionService = permissionService;
        this.groupService = groupService;
    }

    public void addPerm(String groupName, String permissionName, Player player) {
        groupService.findGroupByName(
                Groups.GroupName
                        .newBuilder()
                        .setGroupName(groupName)
                        .build(),
                new ActionableObserver<>(
                        response -> {
                            group = response;
                            findPermissionByName(permissionName, player);
                        },
                        throwable -> {
                            new GrpcExceptionHandler(throwable, "This group not exists", player)
                                    .handleGrpcException();
                        },
                        () -> System.out.println("permiso añadido al grupo")
                )
        );
    }

    public void findPermissionByName(String permissionName, Player player) {
        permissionService.findPermissionByName(
                Permissions.PermissionName.newBuilder()
                        .setPermissionName(permissionName)
                        .build(),
                new ActionableObserver<>(
                        response -> {
                            permission = response;
                            updateGroup(player);
                        },
                        throwable -> {
                            new GrpcExceptionHandler(throwable, "This permission not exists", player)
                                    .handleGrpcException();
                        },
                        () -> System.out.println("permiso añadido al grupo")
                )
        );
    }

    public void updateGroup(Player player) {
        List<Integer> permissionsList = new ArrayList<>(group.getPermissionsList());
        permissionsList.add(permission.getIdPermission());
        groupService.updateGroup(
                Groups.UpdateGroupDto
                        .newBuilder()
                        .setIdGroup(group.getIdGroup())
                        .setPermissions(
                                Groups.PermissionsRelations
                                        .newBuilder()
                                        .setProvided(true)
                                        .setIsEmpty(false)
                                        .addAllIds(permissionsList)
                                        .build())
                        .build(),
                new UpdatedGroupObserver(
                        player,
                        "The permission has been added successfully"
                )
        );
    }
}
