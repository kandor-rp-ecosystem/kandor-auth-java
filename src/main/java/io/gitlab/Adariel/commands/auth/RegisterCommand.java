package io.gitlab.Adariel.commands.auth;

import accounts.Accounts;
import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import com.google.inject.Inject;
import io.gitlab.Adariel.observers.accounts.CreateAccountObserver;
import io.gitlab.Adariel.services.AccountService;
import net.kyori.adventure.text.Component;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;


@SuppressWarnings("unused")
@CommandAlias("register")
@Description("Register a player")
public class RegisterCommand extends BaseCommand {
    private final AccountService accountService;

    @Inject
    public RegisterCommand(AccountService accountService) {
        this.accountService = accountService;
    }

    @Default
    @Syntax("<password> <repeat password>")
    @CommandCompletion("<password> <repeat password>")
    public void onRegister(Player player, @NotNull String password, String repeatPassword) {
        if (!password.equals(repeatPassword)) {
            player.sendMessage(Component.text("Las contraseñas no coinciden"));
            return;
        }
        Accounts.CreateAccountDto account = Accounts.CreateAccountDto
                .newBuilder()
                .setUuid(Objects.requireNonNull(player.getPlayer()).getUniqueId().toString())
                .setNick(player.getPlayer().getName())
                .setLogged(true)
                .setPassword(password)
                .setGroups(
                        Accounts.GroupsRelations
                                .newBuilder()
                                .setProvided(false)
                                .build()
                )
                .build();
        accountService.createAccount(account, new CreateAccountObserver(player));
        //TODO: mirar
        player.sendMessage(Component.text("Te has registrado correctamente"));
    }
}
