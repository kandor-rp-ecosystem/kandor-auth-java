package io.gitlab.Adariel.listeners;

import accounts.Accounts;
import com.google.inject.Inject;
import groups.Groups;
import io.gitlab.Adariel.observers.ActionableObserver;
import io.gitlab.Adariel.services.AccountService;
import io.gitlab.Adariel.services.GroupService;
import io.gitlab.Adariel.services.PermissionService;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import permissions.Permissions;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PlayerJoinListener implements Listener {

    @Inject
    JavaPlugin plugin;

    @Inject
    GroupService groupService;

    Player player;

    @Inject
    PermissionService permissionService;

    @Inject
    AccountService accountService;

    Accounts.Account account;

    Groups.ResponseGroupsList groupsList;

    @EventHandler
    public void onPlayerJoin(@NotNull PlayerJoinEvent event) {
        player = event.getPlayer();
        final String UUID = player.getUniqueId().toString();
        accountService.findAccountByUUID(
                Accounts.AccountId
                        .newBuilder()
                        .setUuid(UUID)
                        .build(),
                new ActionableObserver<>(
                        response -> {
                            account = response;
                            findGroups();
                        },
                        Throwable::printStackTrace,
                        () -> System.out.println("Holis")
                )
        );
    }

    private void addPermission(@NotNull Player player, String permission) {
        PermissionAttachment attachment = player.addAttachment(plugin);
        attachment.setPermission(permission, true);
    }

    private void findGroups() {
        groupService.findGroupByIds(
                Groups.GroupIds
                        .newBuilder()
                        .addAllIds(account.getGroupsList())
                        .setIsProvided(true)
                        .setIsEmpty(account.getGroupsList().isEmpty())
                        .build(),
                new ActionableObserver<>(
                        response -> {
                            groupsList = response;
                            findPermissions();
                        },
                        Throwable::printStackTrace,
                        () -> System.out.println("Todo bien")
                )
        );
    }

    private void findPermissions() {
        List<Integer> permissionIds = groupsList.getGroupsList().stream()
                .flatMap(group -> group.getPermissionsList().stream())
                .collect(Collectors.toList());
        System.out.println(groupsList);
        System.out.println(permissionIds);
        permissionService.findPermissionByIds(
                Permissions.PermissionIds
                        .newBuilder()
                        .addAllIds(permissionIds)
                        .setIsProvided(true)
                        .setIsEmpty(permissionIds.isEmpty())
                        .build(),
                new ActionableObserver<>(
                        response -> {
                            response.getPermissionsList().forEach(
                                    permission -> player.addAttachment(plugin, permission.getName(), true)
                            );
                        },
                        Throwable::printStackTrace,
                        () -> System.out.println("Vamos añadir permisos")
                )
        );
    }
}
