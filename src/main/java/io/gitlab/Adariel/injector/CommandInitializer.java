package io.gitlab.Adariel.injector;

import co.aikar.commands.PaperCommandManager;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import io.gitlab.Adariel.commands.auth.RegisterCommand;
import io.gitlab.Adariel.commands.groups.*;
import io.gitlab.Adariel.services.GroupService;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

@Singleton
public class CommandInitializer extends Thread implements Initializer {
    @Inject
    private JavaPlugin javaPlugin;

    @Inject
    private GroupService groupService;

    @Override
    public void init(@NotNull Injector injector) {
        PaperCommandManager commandManager = new PaperCommandManager(javaPlugin);
        commandManager.registerCommand(injector.getInstance(AddPermToGroupCmd.class));
        commandManager.registerCommand(injector.getInstance(AddPlayerToGroupCmd.class));
        commandManager.registerCommand(injector.getInstance(DeleteGroupCmd.class));
        commandManager.registerCommand(injector.getInstance(GroupCmd.class));
        commandManager.registerCommand(injector.getInstance(GroupCreateCmd.class));
        commandManager.registerCommand(injector.getInstance(RemovePermFromGroupCmd.class));
        commandManager.registerCommand(injector.getInstance(RemovePlayerFromGroupCmd.class));
        commandManager.registerCommand(injector.getInstance(GroupAsOPCmd.class));
        commandManager.registerCommand(injector.getInstance(SetGroupPrefixCmd.class));
        commandManager.registerCommand(injector.getInstance(DeleteGroupPrefixCmd.class));
        commandManager.registerCommand(injector.getInstance(RegisterCommand.class));
        commandManager.registerCommand(injector.getInstance(SetGroupSuffixCmd.class));
        commandManager.registerCommand(injector.getInstance(DeleteGroupSuffixCmd.class));
        //TODO: AUTCOMPLETION REGISTRATION
    }
}
