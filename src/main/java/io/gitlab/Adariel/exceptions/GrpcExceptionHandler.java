package io.gitlab.Adariel.exceptions;

import io.gitlab.Adariel.utils.TextUtils;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import org.bukkit.entity.Player;

public class GrpcExceptionHandler {
    private final Throwable throwable;
    private final String message;
    private final Player player;

    public GrpcExceptionHandler(Throwable throwable, String message, Player player) {
        this.throwable = throwable;
        this.message = message;
        this.player = player;
    }

    public void handleGrpcException() {
        if (this.throwable instanceof StatusRuntimeException) {
            if (((StatusRuntimeException) throwable).getStatus().getCode() == Status.Code.NOT_FOUND) {
                player.sendMessage(TextUtils.createWarningComponent(message));
            }
        }
    }
}
