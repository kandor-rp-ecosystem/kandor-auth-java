package io.gitlab.Adariel;

import com.google.inject.Inject;
import io.gitlab.Adariel.services.PermissionService;
import io.grpc.stub.StreamObserver;
import org.bukkit.Bukkit;
import permissions.Permissions;

import java.util.LinkedHashMap;
import java.util.Map;

public class PermissionsLoader {

    private final PermissionService permissionService;

    @Inject
    public PermissionsLoader(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    public void savePermissions() {
        Map<String, String> permissions = new LinkedHashMap<>();

        permissions.put("*", "Superadmin permission");
        permissions.put("kandor.auth.group.addperm", "Add permission to group");
        permissions.put("kandor.auth.group.addplayer", "Add player to group");
        permissions.put("kandor.auth.group.delete", "Delete a group");
        permissions.put("kandor.auth.group.setop", "Set group as op");
        permissions.put("kandor.auth.group.help", "Show group command help");
        permissions.put("kandor.auth.group.create", "Create a group");
        permissions.put("kandor.auth.group.remove", "Remove perm from group");
        permissions.put("kandor.auth.group.removeplayer", "Remove player from group");
        permissions.put("kandor.auth.group.setprefix", "Add group prefix");
        permissions.put("kandor.auth.group.setsuffix", "Add group suffix");
        permissions.put("kandor.auth.group.deletesuffix", "Delete group suffix");

        final int[] idPermission = {1};
        permissions.forEach((permission, description) -> {
            permissionService.createPermission(
                    Permissions.CreatePermissionDto
                            .newBuilder()
                            .setIdPermission(idPermission[0])
                            .setName(permission)
                            .setDescription(description)
                            .build(),
                    new StreamObserver<>() {
                        @Override
                        public void onNext(Permissions.PermissionId value) {
                            Bukkit.getLogger().info("Creado el permiso: " + value.getIdPermission());
                        }

                        @Override
                        public void onError(Throwable t) {
                            t.printStackTrace();
                        }

                        @Override
                        public void onCompleted() {
                        }
                    }
            );
            idPermission[0]++;
        });
    }
}
