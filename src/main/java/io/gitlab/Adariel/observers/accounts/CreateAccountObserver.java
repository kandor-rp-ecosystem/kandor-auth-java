package io.gitlab.Adariel.observers.accounts;

import accounts.Accounts;
import io.grpc.stub.StreamObserver;
import net.kyori.adventure.text.Component;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;


public class CreateAccountObserver implements StreamObserver<Accounts.AccountId> {

    private final Player player;

    public CreateAccountObserver(Player player) {
        this.player = player;
    }

    @Override
    public void onNext(Accounts.AccountId value) {
        player.sendMessage(Component.text("Cuenta creada"));
    }

    @Override
    public void onError(@NotNull Throwable t) {
        player.sendMessage(Component.text("Ha habido un error al crear tu cuenta"));
        t.printStackTrace();
    }

    @Override
    public void onCompleted() {
        System.out.println("se ha completado el grpc");
    }
}
