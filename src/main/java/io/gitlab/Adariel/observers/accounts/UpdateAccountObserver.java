package io.gitlab.Adariel.observers.accounts;

import accounts.Accounts;
import io.gitlab.Adariel.utils.TextUtils;
import io.grpc.stub.StreamObserver;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class UpdateAccountObserver implements StreamObserver<Accounts.AccountId> {

    private final Player player;
    private final String message;

    public UpdateAccountObserver(Player player, String message) {
        this.player = player;
        this.message = message;
    }

    @Override
    public void onNext(Accounts.AccountId value) {

    }

    @Override
    public void onError(@NotNull Throwable t) {
        t.printStackTrace();
    }

    @Override
    public void onCompleted() {
        player.sendMessage(TextUtils.createSuccessComponent(message));
    }
}
