package io.gitlab.Adariel.observers;

public interface ResponseAction<T> {
    void performAction(T response);
}
