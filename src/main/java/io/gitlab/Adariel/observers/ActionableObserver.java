package io.gitlab.Adariel.observers;

import io.grpc.stub.StreamObserver;

public class ActionableObserver<T> implements StreamObserver<T> {
    private final ResponseAction<T> onNextAction;
    private final ResponseAction<Throwable> onErrorAction;
    private final Runnable onCompletedAction;

    public ActionableObserver(
            ResponseAction<T> onNextAction,
            ResponseAction<Throwable> onErrorAction,
            Runnable onCompletedAction) {
        this.onNextAction = onNextAction;
        this.onErrorAction = onErrorAction;
        this.onCompletedAction = onCompletedAction;
    }

    @Override
    public void onNext(T value) {
        this.onNextAction.performAction(value);
    }

    @Override
    public void onError(Throwable t) {
        this.onErrorAction.performAction(t);
    }

    @Override
    public void onCompleted() {
        this.onCompletedAction.run();
    }
}