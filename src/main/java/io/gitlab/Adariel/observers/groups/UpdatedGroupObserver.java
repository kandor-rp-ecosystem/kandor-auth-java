package io.gitlab.Adariel.observers.groups;

import groups.Groups;
import io.gitlab.Adariel.utils.TextUtils;
import io.grpc.stub.StreamObserver;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class UpdatedGroupObserver implements StreamObserver<Groups.GroupId> {

    private final Player player;
    private final String message;

    public UpdatedGroupObserver(Player player, String message) {
        this.player = player;
        this.message = message;
    }

    @Override
    public void onNext(Groups.GroupId value) {

    }

    @Override
    public void onError(@NotNull Throwable t) {
        t.printStackTrace();
    }

    @Override
    public void onCompleted() {
        player.sendMessage(TextUtils.createSuccessComponent(message));
    }
}
