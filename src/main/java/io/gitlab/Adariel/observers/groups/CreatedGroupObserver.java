package io.gitlab.Adariel.observers.groups;

import groups.Groups;
import io.gitlab.Adariel.utils.TextUtils;
import io.grpc.stub.StreamObserver;
import org.bukkit.entity.Player;

public class CreatedGroupObserver implements StreamObserver<Groups.GroupId> {
    private final Player player;
    private final String message;

    public CreatedGroupObserver(Player player, String message) {
        this.player = player;
        this.message = message;
    }

    @Override
    public void onNext(Groups.GroupId value) {
        System.out.println("Next: Create GroupObserver");
    }

    @Override
    public void onError(Throwable t) {
        t.printStackTrace();
    }

    @Override
    public void onCompleted() {
        player.sendMessage(TextUtils.createSuccessComponent(message));
    }
}
