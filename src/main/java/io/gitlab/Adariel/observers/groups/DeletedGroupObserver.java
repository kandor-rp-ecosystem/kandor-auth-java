package io.gitlab.Adariel.observers.groups;

import com.google.protobuf.Empty;
import io.gitlab.Adariel.utils.TextUtils;
import io.grpc.stub.StreamObserver;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class DeletedGroupObserver implements StreamObserver<Empty> {
    private final Player player;
    private final String message;

    public DeletedGroupObserver(Player player, String message) {
        this.player = player;
        this.message = message;
    }

    @Override
    public void onNext(Empty value) {
        System.out.println("NEXT; grupo borrado");
    }

    @Override
    public void onError(@NotNull Throwable t) {
        t.printStackTrace();
    }

    @Override
    public void onCompleted() {
        player.sendMessage(TextUtils.createSuccessComponent(message));
    }
}
