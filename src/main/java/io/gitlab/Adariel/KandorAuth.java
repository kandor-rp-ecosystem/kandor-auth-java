package io.gitlab.Adariel;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import io.gitlab.Adariel.injector.BukkitBinder;
import io.gitlab.Adariel.injector.CommandInitializer;
import io.gitlab.Adariel.injector.ListenerInitializer;
import org.bukkit.plugin.java.JavaPlugin;

@SuppressWarnings("unused")
public class KandorAuth extends JavaPlugin {
    @Inject
    private ListenerInitializer listenerInitializer;
    @Inject
    private CommandInitializer commandInitializer;

    @Inject
    private PermissionsLoader permissionsLoader;

    @Override
    public void onEnable() {
        final Injector injector = Guice.createInjector(new BukkitBinder(this));
        injector.injectMembers(this);
        listenerInitializer.init(injector);
        commandInitializer.init(injector);
        permissionsLoader.savePermissions();
        saveDefaultConfig();
    }
}