package io.gitlab.Adariel.services.clients;

import lombok.Getter;
import permissions.PermissionsServiceGrpc;

public class PermissionsClient extends GrpcChannel {
    @Getter
    private final PermissionsServiceGrpc.PermissionsServiceStub permissionStub;

    public PermissionsClient() {
        this.permissionStub = PermissionsServiceGrpc.newStub(this.getManagedChannel());
    }
}
