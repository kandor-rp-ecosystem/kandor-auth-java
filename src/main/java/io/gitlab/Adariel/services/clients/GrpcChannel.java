package io.gitlab.Adariel.services.clients;

import com.google.inject.Singleton;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.Getter;

@Singleton
public class GrpcChannel {
    @Getter
    private final ManagedChannel managedChannel = ManagedChannelBuilder
            .forTarget("127.0.0.1:50051")
            .usePlaintext()
            .build();
}
