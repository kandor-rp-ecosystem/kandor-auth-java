package io.gitlab.Adariel.services.clients;

import accounts.AccountsServiceGrpc;
import lombok.Getter;

public class AccountClient extends GrpcChannel {
    @Getter
    private final AccountsServiceGrpc.AccountsServiceStub accountStub;


    public AccountClient() {
        accountStub = AccountsServiceGrpc.newStub(this.getManagedChannel());
    }
}
