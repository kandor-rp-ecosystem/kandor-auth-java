package io.gitlab.Adariel.services.clients;

import groups.GroupsServiceGrpc;
import lombok.Getter;

public class GroupClient extends GrpcChannel {
    @Getter
    private final GroupsServiceGrpc.GroupsServiceStub groupStub;

    public GroupClient() {
        groupStub = GroupsServiceGrpc.newStub(this.getManagedChannel());
    }
}
