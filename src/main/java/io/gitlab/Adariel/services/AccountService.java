package io.gitlab.Adariel.services;

import accounts.Accounts;
import accounts.AccountsServiceGrpc;
import com.google.inject.Inject;
import io.gitlab.Adariel.services.clients.AccountClient;
import io.grpc.stub.StreamObserver;
import org.jetbrains.annotations.NotNull;

//TODO: crear nuevos observadores
public class AccountService {
    private final AccountsServiceGrpc.AccountsServiceStub accountStub;

    @Inject
    public AccountService(@NotNull AccountClient accountClient) {
        this.accountStub = accountClient.getAccountStub();
    }

    public void createAccount(@NotNull Accounts.CreateAccountDto account, StreamObserver<Accounts.AccountId> observer) {
        accountStub.create(account, observer);
    }

    public void updateAccount(Accounts.UpdateAccountDto account, StreamObserver<Accounts.AccountId> observer) {
        accountStub.update(account, observer);
    }

    public void findAccountByUUID(Accounts.AccountId uuid, StreamObserver<Accounts.Account> observer) {
        accountStub.findById(uuid, observer);
    }

    public void findAccountByUUIDs(Accounts.AccountIds uuids, StreamObserver<Accounts.ResponseAccountsList> observer) {
        accountStub.findByIds(uuids, observer);
    }

    public void findAccountByNick(Accounts.AccountNick nick, StreamObserver<Accounts.Account> observer) {
        accountStub.findByNick(nick, observer);
    }

    public void deleteAccount(Accounts.AccountId uuid, StreamObserver<Accounts.Account> observer) {
        accountStub.findById(uuid, observer);
    }
}
