package io.gitlab.Adariel.services;

import com.google.inject.Inject;
import com.google.protobuf.Empty;
import groups.Groups;
import groups.GroupsServiceGrpc;
import io.gitlab.Adariel.services.clients.GroupClient;
import io.grpc.stub.StreamObserver;
import org.jetbrains.annotations.NotNull;

//TODO: Sacar el cliente a una unica variable
public class GroupService {

    private final GroupsServiceGrpc.GroupsServiceStub groupStub;


    @Inject
    public GroupService(@NotNull GroupClient groupClient) {
        this.groupStub = groupClient.getGroupStub();
    }

    public void createGroup(@NotNull Groups.CreateGroupDto group, StreamObserver<Groups.GroupId> createGroupObserver) {
        groupStub.create(group, createGroupObserver);
    }

    public void updateGroup(Groups.UpdateGroupDto group, StreamObserver<Groups.GroupId> createGroupObserver) {
        groupStub.update(group, createGroupObserver);
    }

    public void findAllGroups(StreamObserver<Groups.ResponseGroupsList> observer) {
        groupStub.findAll(Empty.newBuilder().build(), observer);
    }

    public void findGroupById(Groups.GroupId idGroup, StreamObserver<Groups.Group> createGroupObserver) {
        groupStub.findById(idGroup, createGroupObserver);
    }

    public void findGroupByIds(Groups.GroupIds idGroups, StreamObserver<Groups.ResponseGroupsList> createGroupObserver) {
        groupStub.findByIds(idGroups, createGroupObserver);
    }

    public void findGroupByName(Groups.GroupName groupName, StreamObserver<Groups.Group> createGroupObserver) {
        groupStub.findByName(groupName, createGroupObserver);
    }

    public void deleteGroup(Groups.GroupId groupId, StreamObserver<Empty> createGroupObserver) {
        groupStub.remove(groupId, createGroupObserver);
    }

}
