package io.gitlab.Adariel.services;

import com.google.inject.Inject;
import io.gitlab.Adariel.services.clients.PermissionsClient;
import io.grpc.stub.StreamObserver;
import org.jetbrains.annotations.NotNull;
import permissions.Permissions;
import permissions.PermissionsServiceGrpc;

public class PermissionService {
    private final PermissionsServiceGrpc.PermissionsServiceStub permissionStub;

    @Inject
    public PermissionService(PermissionsClient permissionsClient) {
        this.permissionStub = permissionsClient.getPermissionStub();
    }

    public void createPermission(
            @NotNull Permissions.CreatePermissionDto permission,
            StreamObserver<Permissions.PermissionId> observer) {
        permissionStub.create(permission, observer);
    }

    public void updatePermission(
            @NotNull Permissions.UpdatePermissionDto permission,
            StreamObserver<Permissions.PermissionId> observer
    ) {
        permissionStub.update(permission, observer);
    }

    public void findPermissionByName(
            @NotNull Permissions.PermissionName permission,
            StreamObserver<Permissions.Permission> observer
    ) {
        permissionStub.findByName(permission, observer);
    }

    public void findPermissionById(
            @NotNull Permissions.PermissionId permission,
            StreamObserver<Permissions.Permission> observer
    ) {
        permissionStub.findById(permission, observer);
    }

    public void findPermissionByIds(
            @NotNull Permissions.PermissionIds permissionIds,
            StreamObserver<Permissions.ResponsePermissionsList> observer
    ) {
        permissionStub.findByIds(permissionIds, observer);
    }
}
