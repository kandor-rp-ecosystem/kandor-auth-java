package io.gitlab.Adariel.utils;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.jetbrains.annotations.NotNull;

public class TextUtils {
    public static @NotNull Component createWarningComponent(String message) {
        return Component.text(message, NamedTextColor.YELLOW)
                .decoration(TextDecoration.BOLD, true);
    }

    public static @NotNull Component createSuccessComponent(String message) {
        return Component.text(message, NamedTextColor.GREEN)
                .decoration(TextDecoration.BOLD, true);
    }

    public static @NotNull Component createErrorComponent(String message) {
        return Component.text("ERROR: ", NamedTextColor.RED)
                .decoration(TextDecoration.BOLD, true)
                .append(Component.text(message, NamedTextColor.RED));
    }

    public static @NotNull Component createPlainComponent(String message) {
        return Component.text(message);
    }
}
